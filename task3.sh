#!/bin/bash
awk -F: '$3>=1000' /etc/passwd > /tmp/GroupsAndUsers.txt
awk -F: '$3>=1000' /etc/group >> /tmp/GroupsAndUsers.txt
awk -F: '$6 !=""' /tmp/GroupsAndUsers.txt | cut -d : -f 1 > /tmp/usernames.txt
awk -F: '$6 !=""' /tmp/GroupsAndUsers.txt | cut -d : -f 3 > /tmp/count.txt
echo "Amount Of Users: 3" > /tmp/count.txt
echo "Amount Of Groups: 3" >> /tmp/count.txt